﻿Public Class MainForm

    Private Sub AideOuContactToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AideOuContactToolStripMenuItem.Click
        ' Pour affiche form de l'aide ou contact
        HelpForm.Show()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' TB1 EST R, TB2 EST I, TB3 EST U
        TextBox3.Text = Val(TextBox1.Text) * Val(TextBox2.Text)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        TextBox4.Text = Val(TextBox6.Text) / Val(TextBox5.Text)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        TextBox7.Text = Val(TextBox9.Text) / Val(TextBox8.Text)
    End Sub

    Private Sub AnalyseCircuitÀCCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnalyseCircuitÀCCToolStripMenuItem.Click
        ACCC.Show()
    End Sub

    Private Sub AnalyseCircuitÀCAToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnalyseCircuitÀCAToolStripMenuItem.Click
        ACCA.Show()
    End Sub

    Private Sub ReleverAméliorerLeFacteurDePuissanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReleverAméliorerLeFacteurDePuissanceToolStripMenuItem.Click
        RAFP.Show()
    End Sub

    Private Sub CircuitÉlectroniqueAnalogiqueToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CircuitÉlectroniqueAnalogiqueToolStripMenuItem.Click
        CEA.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
        Exit Sub
    End Sub
End Class
